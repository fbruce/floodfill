// FloodFill.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "FloodFill.h"


const int arraySize = 5; 

FloodFill::FloodFill() {}

FloodFill::~FloodFill() {}

int testArray[arraySize][arraySize] =
{
	0,0,1,0,0,
	0,0,1,0,0,
	0,0,1,0,0,
	0,0,1,0,0,
	0,0,1,0,0
};

void FloodFill::floodFillR(int a[arraySize][arraySize], int x,int y, int oldNum, int newNum)
{
	//if outwith the bounds of the array 
	if (x < 0 || x >= arraySize || y < 0 || y >= arraySize)
		return;
	//if the current number is not the same as the old number
	if (a[x][y] != oldNum)
		return;

	//change the old number to the new number
	a[x][y] = newNum;

	//call flood fill again for right, left, down and up
	floodFillR(a, x + 1, y, oldNum, newNum);
	floodFillR(a, x - 1, y, oldNum, newNum);
	floodFillR(a, x, y + 1, oldNum, newNum);
	floodFillR(a, x, y - 1, oldNum, newNum);
}

void FloodFill::floodFill(int a[arraySize][arraySize], int x, int y, int newNum)
{
	//set the old number to the number in the current position
	int oldNum = a[x][y];
	//call the recursive flood fill function
	floodFillR(a, x, y, oldNum, newNum);
}
int main()
{
	//used for testing to console
	/*std::cout << "Array before filling " << std::endl;
	for (int i = 0; i < arraySize; i++)
	{
		for (int j = 0; j < arraySize; j++)
		{
			std::cout << testArray[i][j] << ' ';
		}
		std::cout << std::endl;
	}*/
	int x = 0, y = 4, newNum = 5;

	FloodFill example;

	example.floodFill(testArray, x, y, newNum);

	//used for testing to console
	/*
	std::cout << "Array after filling " << std::endl;
	for (int i = 0; i < arraySize; i++)
	{
		for (int j = 0; j < arraySize; j++)
		{
			std::cout << testArray[i][j] << ' ';
		}
		std::cout << std::endl;
	}*/

	system("pause");
    return 0;
}

