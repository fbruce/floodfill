#include "stdafx.h"
#include "CppUnitTest.h"
#include "FloodFill.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

FloodFill test;
const int arraySize = 5;


namespace FloodFillTest
{		
	TEST_CLASS(FloodFill)
	{
	public:
		TEST_METHOD(TestArray1_NotEqual)
		{
			//checks the new number isn't equal in times in the array before flood fill and after
			int originalNumCount = 0;
			int newNumCount = 0;
			int testArray[arraySize][arraySize] =
			{
				0,0,1,0,0,
				0,0,1,0,0,
				0,0,1,0,0,
				0,0,1,0,0,
				0,0,1,0,0
			};
			//start position and new number
			int x = 0;
			int y = 2;
			int newNum = 3;

			//count number of instances of new number in initial array
			for (int i = 0; i < arraySize; i++)
				for (int j = 0; j < arraySize; j++)
				{
					if (testArray[i][j] == newNum)
					{
						originalNumCount++;
					}
				}

			//call floodfill
			test.floodFill(testArray, x, y, newNum);

			//count number of instances of new number in new array
			for (int i = 0; i < arraySize; i++)
				for (int j = 0; j < arraySize; j++)
				{
					if (testArray[i][j] == newNum)
					{
						newNumCount++;
					}
				}

			Assert::AreNotEqual(originalNumCount, newNumCount);
		}
		TEST_METHOD(TestArray1_ExpectedOutcome)
		{
			//Checks the number of times the new number is present against a predetermined expected outcome
			int originalNumCount = 0;
			int newNumCount = 0;
			int expectedOutcome = 5;
			int testArray[arraySize][arraySize] =
			{
				0,0,1,0,0,
				0,0,1,0,0,
				0,0,1,0,0,
				0,0,1,0,0,
				0,0,1,0,0
			};
			int x = 0;
			int y = 2;
			int newNum = 3;

			test.floodFill(testArray, x, y, newNum);

			//count number of instances of new number in new array
			for (int i = 0; i < arraySize; i++)
				for (int j = 0; j < arraySize; j++)
				{
					if (testArray[i][j] == newNum)
					{
						newNumCount++;
					}
				}

			Assert::AreEqual(expectedOutcome, newNumCount);
		}
		TEST_METHOD(TestArray2_NotEqual)
		{
			//as before, checking the numbers before and after flood fill but with a new array pattern
			int originalNumCount = 0;
			int newNumCount = 0;
			int testArray[arraySize][arraySize] =
			{
				0,0,1,0,0,
				0,0,1,0,0,
				1,1,1,1,1,
				0,0,1,0,0,
				0,0,1,1,1
			};
			int x = 1;
			int y = 1;
			int newNum = 3;

			//count number of instances of new number in initial array
			for (int i = 0; i < arraySize; i++)
				for (int j = 0; j < arraySize; j++)
				{
					if (testArray[i][j] == newNum)
					{
						originalNumCount++;
					}
				}

			test.floodFill(testArray, x, y, newNum);

			//count number of instances of new number in new array
			for (int i = 0; i < arraySize; i++)
				for (int j = 0; j < arraySize; j++)
				{
					if (testArray[i][j] == newNum)
					{
						newNumCount++;
					}
				}

			Assert::AreNotEqual(originalNumCount, newNumCount);
		}
		TEST_METHOD(TestArray2_ExpectedOutcome)
		{
			//Checks the number of times the new number is present against a predetermined expected outcome
			int originalNumCount = 0;
			int newNumCount = 0;
			int testArray[arraySize][arraySize] =
			{
				0,0,1,0,0,
				0,0,1,0,0,
				1,1,1,1,1,
				0,0,1,0,0,
				0,0,1,1,1
			};
			int x = 2;
			int y = 0;
			int newNum = 3;
			int expectedOutcome = 11;

			test.floodFill(testArray, x, y, newNum);

			//count number of instances of new number in new array
			for (int i = 0; i < arraySize; i++)
				for (int j = 0; j < arraySize; j++)
				{
					if (testArray[i][j] == newNum)
					{
						newNumCount++;
					}
				}
			Assert::AreEqual(expectedOutcome, newNumCount);
		}
		TEST_METHOD(TestArray3_NotEqual)
		{
			//as before, checking the numbers before and after flood fill but with a new array pattern
			int originalNumCount = 0;
			int newNumCount = 0;
			int testArray[arraySize][arraySize] =
			{
				0,2,1,0,0,
				0,2,2,2,4,
				1,1,1,2,2,
				0,5,1,1,2,
				2,2,2,2,2
			};
			int x = 0;
			int y = 1;
			int newNum = 6;

			//count number of instances of new number in initial array
			for (int i = 0; i < arraySize; i++)
				for (int j = 0; j < arraySize; j++)
				{
					if (testArray[i][j] == newNum)
					{
						originalNumCount++;
					}
				}

			test.floodFill(testArray, x, y, newNum);

			//count number of instances of new number in new array
			for (int i = 0; i < arraySize; i++)
				for (int j = 0; j < arraySize; j++)
				{
					if (testArray[i][j] == newNum)
					{
						newNumCount++;
					}
				}

			Assert::AreNotEqual(originalNumCount, newNumCount);
		}
		TEST_METHOD(TestArray3_ExpectedOutcome)
		{
			//Checks the number of times the new number is present against a predetermined expected outcome
			int originalNumCount = 0;
			int newNumCount = 0;
			int testArray[arraySize][arraySize] =
			{
				0,2,1,0,0,
				0,2,2,2,4,
				1,1,1,2,2,
				0,5,1,1,2,
				2,2,2,2,2
			};
			int x = 0;
			int y = 1;
			int newNum = 6;
			int expectedOutcome = 12;

			test.floodFill(testArray, x, y, newNum);

			//count number of instances of new number in new array
			for (int i = 0; i < arraySize; i++)
				for (int j = 0; j < arraySize; j++)
				{
					if (testArray[i][j] == newNum)
					{
						newNumCount++;
					}
				}
			Assert::AreEqual(expectedOutcome, newNumCount);
		}

	};
}