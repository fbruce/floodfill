#pragma once

class FloodFill
{
private:
	const static int arraySize = 5;

public: 
	FloodFill();
	~FloodFill();
	void floodFillR(int a[arraySize][arraySize], int x, int y, int oldNum, int newNum);
	void floodFill(int a[arraySize][arraySize], int x, int y, int newNum);
};